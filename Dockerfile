FROM php:8-alpine

RUN apk add mysql mysql-client

RUN docker-php-ext-install mysqli

WORKDIR /app

RUN chown -R mysql:mysql /app

RUN mkdir -p /run/mysqld

RUN chown -R mysql:mysql /run/mysqld

RUN mkdir -p /var/lib/mysql

RUN chown -R mysql:mysql /var/lib/mysql
